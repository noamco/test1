// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyB_KbVkoymtOIWS0hinKYlABQJphZ8D6SU",
    authDomain: "test1-acab6.firebaseapp.com",
    databaseURL: "https://test1-acab6.firebaseio.com",
    projectId: "test1-acab6",
    storageBucket: "test1-acab6.appspot.com",
    messagingSenderId: "951376989010",
    appId: "1:951376989010:web:62a64cd06c1338dfe7488b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
