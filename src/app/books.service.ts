import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { tap, catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class BooksService {
  
  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'A Song Of Ice And Fire', author:'George R R Martin'}]
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  bookCollection: AngularFirestoreCollection;
 
  constructor(private db:AngularFirestore) { }

  // getBooks(){

  //   const booksObservable = new Observable(
  //     observer => {
  //       setInterval(
  //         () => observer.next(this.books),5000
  //       )
  //     }
  //   )
  //   return booksObservable;
  // }

  getBooks(userId:string):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'});
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        )
  
      )
    )
  }
  
  
  addBook(userId:string, title:string, author:string){
    const book = {title:title, author:author}
    // this.db.collection('books').add(book);
    this.userCollection.doc(userId).collection('books').add(book);
  }
  //delete book for everyone
  // deleteBook(id:string){
  //   this.db.doc(`books/${id}`).delete();
  // }
  deleteBook(id:string, userId:string){
    this.db.doc(`users/${userId}/books/${id}`).delete();
  }

  getBook(id:string, userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get()
  }
  
  //update all books
  // updateBook(id:string, title:string, author:string){
  //   this.db.doc(`books/${id}`).update({
  //     title:title,
  //     author:author
  //   })
  // }

  //update books per user
  updateBook(userId:string, id:string, title:string, author:string){
    this.db.doc(`users/${userId}/books/${id}`).update({
      title:title,
      author:author
    })
  }
  updateAuthor(userId:string, id:string, author:string){
    this.db.doc(`users/${userId}/books/${id}`).update({
      author:author
    })
  }

}



