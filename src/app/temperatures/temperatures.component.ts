import { Component, OnInit } from '@angular/core';
import { Weather } from '../interfaces/weather';
import { Observable, throwError } from 'rxjs';
import { TempService } from '../temp.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  tempData$: Observable<Weather>;
  image:String;
  city;
  temperature;
  likes:number=0;
  errorMessage:String;
  hasError:Boolean = false;
  

  constructor(private route: ActivatedRoute, private tempService:TempService ) { }

  addLikes(){
    this.likes ++ //likes that belongs to this section...
  }


  ngOnInit() {
    this.city= this.route.snapshot.params.city;
    this.tempData$ = this.tempService.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      },
      error =>{
      this.errorMessage = "city not found";
      this.hasError = true;

       console.log(error.message);
      }
  
    )

  }

}
