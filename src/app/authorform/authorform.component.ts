import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-authorform',
  templateUrl: './authorform.component.html',
  styleUrls: ['./authorform.component.css']
})
export class AuthorformComponent implements OnInit {

  constructor(private booksservice:BooksService, 
    private router:Router,
    private route:ActivatedRoute,
    private authservice:AuthService) { }


title:string;
author:string;
id:string;
isEdit:boolean = false;
buttonText:string = "Add Book";
userId: string;

onSubmit(){
  if(this.isEdit){
    this.booksservice.updateAuthor(this.userId, this.id, this.author);
  }
  else{
    this.booksservice.addBook(this.userId, this.title, this.author)
  }
 // this.booksservice.addBook(this.title, this.author)
  this.router.navigate(['/authors']);
}


ngOnInit() {
  this.id = this.route.snapshot.params.id;
  this.authservice.getUser().subscribe(
    user => {
      this.userId = user.uid;    
      if(this.id) {
        this.isEdit = true;
        this.buttonText = 'Update book'
        this.booksservice.getBook(this.id, this.userId).subscribe(
        book => {
          console.log(book.data().author)
          console.log(book.data().title)
          this.author = book.data().author;
          this.title = book.data().title;})        
    }
 })
}
  
}




