import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //  Get User or Null
  user: Observable<User | null>


  // אם מייצרים אתחול של משתנה , כדאי שזה יהיה בתוך הבנאי
  constructor(public afAuth:AngularFireAuth,
               private router:Router) { 
    this.user = this.afAuth.authState;
  }

  getUser(){
    return this.user
  }

  SignUp(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(user => {
        this.router.navigate (['/books'])
      })
  }


  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu', res));
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(res => console.log(this.user.subscribe(user=>
      console.log(user.uid))))
  }



}