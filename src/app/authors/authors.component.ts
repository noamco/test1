import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  books: any;
  panelOpenState = false;
  books$:Observable<any>;
  userId:string;

  constructor(private booksservice:BooksService,
    public authservice:AuthService) { }
    
    ngOnInit() {
      /*
      this.books = this.booksservice.getBooks().subscribe(
        (books) => this.books = books
      )
      
     //this.booksservice.addBooks();
      this.books$ = this.booksservice.getBooks();
      */
     this.authservice.user.subscribe(
       user => {
         this.userId = user.uid;
         this.books$ = this.booksservice.getBooks(this.userId);
       }
     )
    }  

}
