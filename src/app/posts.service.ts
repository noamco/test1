import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Posts } from './interfaces/posts';

@Injectable({
  providedIn: 'root'
})


export class PostsService {

  private API = "https://jsonplaceholder.typicode.com/posts"

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Posts>
  {
  
    return this.http.get<Posts>(`${this.API}`);
   
  }
 
}
